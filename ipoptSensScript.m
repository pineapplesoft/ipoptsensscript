%% Script for generating ampl ipopt_sens declerations as options
%  -Written by: Chriss Grimholt, July 2012.
 
%% User should define here the variables (disturbances) that will change.
% IMPORTANT: all the disturbances must be declared as variable in your AMPL
% model

%List of disturbance names 
namVar = {'add','disturbance','variables'};

%Here you include the nominal values of the disturbance
nominal_values=[1 1 1];

%Here you include the perturbed values of the disturbance
perturbed_values=[2 2 2];


%% From now on we take care of everything ( only modifiy below if you know what your are doing)

numVar = length(namVar);
%generates disturbance values data file
fID = fopen('runIpoptSens.dat','w');
fprintf(fID, 'param:T: nominal  perturbed:=\n');

for i=1:numVar
   fprintf(fID,'\t %i \t %f \t %f \t #%s \n',i,nominal_values(i),perturbed_values(i),namVar{i}); 
end
fprintf(fID, ';');

fclose(fID);

% genetrates the suffix declerations
fID = fopen('runIpoptSens.inc','w');
fprintf(fID, '# Usage: put the command: commands ipopt_sens.run \n# into your model file before solve\n\n');
fprintf(fID, '# Suffixes for sensitivity update \n');
for i = 0:numVar;
    fprintf(fID,'suffix sens_state_%i, IN;\n',i);
    if i~= 0
        fprintf(fID,'suffix sens_state_value_%i, IN;\n',i);
        fprintf(fID,'suffix sens_sol_state_%i, OUT;\n',i);
    end
end
fprintf(fID, 'suffix sens_init_constr, IN;\n\n');

% generates the ipopt options
fprintf(fID, '# Solver options \n');
fprintf(fID, 'option solver ipopt_sens;\n');
fprintf(fID, 'option presolve 0;\n');
fprintf(fID, 'option ipopt_options ''run_sens yes'';\n\n');

 
fprintf(fID, '# decleration of peturbed variables \n');
fprintf(fID, 'set T; #:={0..%i};\n',numVar);
fprintf(fID, '# var Var{T}; # must be declared in the model\n');
fprintf(fID, 'param perturbed{T};\n');
fprintf(fID, 'param nominal{T};\n\n');

% generates the auxiliary constraints
fprintf(fID, '# auxiliary constraints for parameters \n');
for i = 1:numVar;
    fprintf(fID, 'auxCon%i: %s=nominal[%i]; \n',i,namVar{i},i);
end

% loads the data file with the nominal and perturbed data 
fprintf(fID, '# data for nominal and perturbed Var\n');
fprintf(fID, 'data runIpoptSens.dat;\n\n');


% generates the pertubation matrix
fprintf(fID, '# ipopt_sens data\n'); 
for i = 0:numVar;
   for k = 1:numVar
       fprintf(fID, 'let  %s.sens_state_%i := %i;\n',namVar{k},i,k);
   end
   if i ~= 0; 
      for j = 1:numVar
          if j == i;
              % diagonal perturbation
              fprintf(fID, 'let %s.sens_state_value_%i := perturbed[%i];\n',namVar{i},i,i);
          else
              % off-diagonal nominal values
              fprintf(fID, 'let %s.sens_state_value_%i := nominal[%i];\n',namVar{j},i,j);
          end
      end
   end
end

 
for i = 1:numVar;
    fprintf(fID,'\nlet auxCon%i.sens_init_constr :=1;',i);
end
fclose(fID);

%% Generates the option file for ipopt_sens

fID = fopen('ipopt.opt','w');
fprintf(fID,'n_sens_steps %i\n',numVar); % tells the solver that there is numVar perturbations
fprintf(fID,'sens_boundcheck no\n');
fprintf(fID,'#sens_bound_eps -1e-10\n');
fprintf(fID,'sens_allow_inexact_backsolve yes');
fclose(fID);

%% Generates ampl code for printing the ipopt_sens results
%  -Written by: Vinicius de Oliveira, July 2012.

fID = fopen('saveIpoptSens.inc','w');
% creates the code for saveing the pertubation data
for i = 0:numVar
  if i==0
      fprintf(fID,'%s\n', 'printf "% Nominal solution: y= ">data.m;');
      fprintf(fID,'%s\n', 'print {t  in 1.. _nvars} _varname[t] >> data.m;');
      fprintf(fID,'%s\n', 'printf "ynom=[" >data.m;');
      fprintf(fID,'%s\n', 'print  {t in 1.._nvars} _var[t] >>data.m;');
      fprintf(fID,'%s\n\n', 'printf "]'';\n\n" >data.m;');
  else
      fprintf(fID,'printf "%%%% d_%i= %%s  perturbed- %%s nominal",''%s'',''%s'' >data.m;\n',i,namVar{i},namVar{i});
       fprintf(fID,'%s\n', 'printf "\n">data.m;');
      fprintf(fID,'param d_%i:= perturbed[%i]-nominal[%i];\n\n',i,i,i);
      fprintf(fID,'printf "d_%i= %%5.5f;",d_%i>data.m;',i,i);
      fprintf(fID,'%s\n', 'printf "\n">data.m;');
      fprintf(fID,'printf "%% Solution with disturbance %%s :y= ",''%s''>data.m;\n',namVar{i});
      fprintf(fID,'%s\n', 'print {t  in 1.. _nvars} _varname[t] >> data.m;');
      fprintf(fID,'printf "yd%i=[" >data.m;\n',i);
      fprintf(fID,'print  {t in 1.._nvars} _var[t].sens_sol_state_%i >>data.m; \n',i);
      fprintf(fID,'%s\n\n', 'printf "]'';\n\n" >data.m;');
  end
end

% generating the code for calculating the F (optimal sensitivity) matrix
fprintf(fID,'printf "F=[];">data.m;');
fprintf(fID,'%s\n', 'printf "\n">data.m;');
fprintf(fID,'printf "for i=1:%i " >data.m;',numVar);
fprintf(fID,'%s\n', 'printf "\n">data.m;');
fprintf(fID,'printf "d=sprintf(''d_%%%%i'',i);" >data.m;');
fprintf(fID,'%s\n', 'printf "\n">data.m;');
fprintf(fID,'printf "yd=sprintf(''yd%%%%i'',i);" >data.m;');
fprintf(fID,'%s\n', 'printf "\n">data.m;');
fprintf(fID,'printf "F=[F (eval(yd)-ynom)/eval(d) ];" >data.m;');
fprintf(fID,'%s\n', 'printf "\n">data.m;');
fprintf(fID,'%s\n', 'printf "end">data.m;');

fclose(fID);



