A Matlab script for generating pre and post codes for AMPL models using sIpopt to generate the optimal sensitivities.

Four files are generated:

* runIpoptSens.inc -> contains necessary AMPL definitions to run sIpopt

* runIpoptSens.dat -> AMPL data file with nominal and perturbed values of disturbances 

* ipopt.opt  -> option file for ipopt_sens

* saveIpoptSens.inc -> saves all variables to data.m which computes optimal sensitivities

After you run the script, please copy the 4 files to your AMPL working folder
and include runIpoptSens.inc and saveIpoptSens.inc to your main AMPL file, as shown
below.

Example of use :

#IMPORTANT: all the disturbances must be declared as variable in your AMPL model	

#This is your main AMPL script

       model relevant_system.mod; (change param disturbances; -> var disturbances;)
       data data.dat;      
      -> include runIpoptSens.inc;
       solve;
      -> include saveIpoptSens.inc;
      
 #After this,  all the relevant data will be stored in data.m
